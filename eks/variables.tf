variable "aws_region" { }
variable "project_name" { }
variable "component_label_eks" { }
variable "environment" { }

variable "user-arn" { }
variable "user-name" { }

variable "vpc-cidr" { }
variable "vpc-azs" { type = "list" }
variable "vpc-public-subnets" { type = "list" }
variable "vpc-private-subnets" { type = "list" }
variable "vpc-intra-subnets" { type = "list" }

variable "k8s-version" { }
variable "k8s-worker-instance0-type" { }
variable "k8s-worker-instance0-amiid" { }
variable "k8s-worker-instance1-type" { }
variable "k8s-worker-instance1-amiid" { }

variable "hostedzone-zone_id" { }
