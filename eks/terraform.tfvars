
project_name = "kungfu-eks-test"
component_label_eks = "kungfu-eks-test"
environment = "test"
aws_region = "us-east-2"

user-arn = "arn:aws:iam::111111111111:user/user@domain.com"
user-name = "user@domain.com"

vpc-cidr = "10.69.0.0/16"
vpc-azs = ["us-east-2a","us-east-2b"]
vpc-public-subnets = ["10.69.69.0/24","10.69.70.0/24"] 
vpc-private-subnets = ["10.69.71.0/24","10.69.72.0/24"]
vpc-intra-subnets = ["10.69.4.0/24","10.69.5.0/24"]

k8s-version = "1.13"
k8s-worker-instance0-type = "t3.medium"
k8s-worker-instance0-amiid = "ami-07f54fc065c3da9d7"
k8s-worker-instance1-type = "t3.medium"
k8s-worker-instance1-amiid = "ami-07f54fc065c3da9d7"

hostedzone-zone_id = "YOUR-HOSTEDZONE-ID"

