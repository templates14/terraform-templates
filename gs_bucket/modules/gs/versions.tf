terraform {
  required_version = "= 0.12.28"

  required_providers {
    # contrain the provider version to avoid errors between runner executions
    google-beta = "~> 3.14"
    google      = "~> 3.14"
    null        = "~> 2.1"
    random      = "~> 2.2"
  }
}
